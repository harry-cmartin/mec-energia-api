import pytest
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType

def test_user_type_validation():
    # Test invalid user type for UniversityUser
    user_type_invalid = 'not_defined'
    expected_error_message_invalid = f'User type ({user_type_invalid}) does not exist'

    with pytest.raises(Exception) as context_invalid:
        UserType.is_valid_user_type(user_type_invalid, user_model=UniversityUser)

    actual_error_message_invalid = str(context_invalid.value)

    assert actual_error_message_invalid == expected_error_message_invalid, f"Esperado: {expected_error_message_invalid}, Obtido: {actual_error_message_invalid}"

    # Test invalid user type for CustomUser
    user_type_super_user = 'super_user'
    expected_error_message_super_user = f"Wrong User type ({user_type_super_user}) for this Model User (<class 'users.models.UniversityUser'>)"

    with pytest.raises(Exception) as context_super_user:
        UserType.is_valid_user_type(user_type_super_user, user_model=UniversityUser)
    
    actual_error_message_super_user = str(context_super_user.value)

    assert actual_error_message_super_user == expected_error_message_super_user, f"Esperado: {expected_error_message_super_user}, Obtido: {actual_error_message_super_user}"

    # Test invalid user type for CustomUser
    user_type_custom_user = 'university_user'
    expected_error_message_custom_user = f"Wrong User type ({user_type_custom_user}) for this Model User (<class 'users.models.CustomUser'>)"

    with pytest.raises(Exception) as context_custom_user:
        UserType.is_valid_user_type(user_type_custom_user, user_model=CustomUser)
    
    actual_error_message_custom_user = str(context_custom_user.value)

    assert actual_error_message_custom_user == expected_error_message_custom_user, f"Esperado: {expected_error_message_custom_user}, Obtido: {actual_error_message_custom_user}"